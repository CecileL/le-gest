const express = require("express");
const mysql = require("mysql");
const bodyParser = require('body-parser');
const app = express();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt-nodejs');
const cookieParser = require('cookie-parser');
/*const connect = require('connect');*/

app.use(bodyParser.json());
var urlencodeparser = bodyParser.urlencoded({ extended: true });
app.use(urlencodeparser);
app.use(cookieParser());

const db = mysql.createConnection({
    host: '127.0.0.1',
    user: 'carbonne_gest2',
    password: 'TxadfUBj',
    database: 'carbonne_gest2'
});

db.connect(function (err) {
    if (err) throw err;
    console.log("mysql connecté");
});

app.set('view engine', 'ejs');

/*Création des chemins*/
app.get("/", function (req, res) {
    res.render('accueil');
});
app.get("/connexion", function (req, res) {
    res.render('connexion');
});
app.get("/invite", function (req, res) {
    res.render('invite');
});
app.get("/menu", function (req, res) {
    res.render('menu');
});
app.get("/annuaire", function (req, res) {
    res.render('annuaire');
});
app.get("/agenda", function (req, res) {
    res.render('agenda');
});
app.get("/actualite", function (req, res) {
    res.render('actualite');
});
app.get("/forum", function (req, res) {
    res.render('forum');
});
app.get("/ajouter", function (req, res) {
    res.render('ajouter');
});
app.get("/fiche", function (req, res) {
    res.render('fiche');
});
app.get("/actualites", function (req, res) {
    res.render('actualites');
});
app.get("/actugest", function (req, res) {
    res.render('actugest');
});
app.get("/actumembres", function (req, res) {
    res.render('actumembres');
});
app.get("/forum", function (req, res) {
    res.render('forum');
});
app.get("/reunion", function (req, res) {
    res.render('reunion')
});
app.get("/createevent", function (req, res){
    res.render('createevent');
});
app.post("/creationevent", function (req, res){
    res.render('agenda');
})
app.get("/presta", function (req, res){
    res.render('presta');
})

app.get("/participants", function(req, res){
    res.render('participants');
});
app.get("/evenement", function (req, res) {
    res.render('evenement');
});

/*Fin des chemins*/

app.use(express.static("public"));
app.use(passport.initialize());
app.use(passport.session());

//Fiche d'entreprise

app.post("/users/:users_id", function (req, res) {
    db.query('SELECT * FROM users WHERE id=' + req.params.users_id, function (error, results, fields) {
        if (!error) {
            res.render('fiche', { user: results[0]});
        }
    })
})

let message = "";
let logged_user = "";

app.get("/ajouter", function (req, res) {
    res.render("ajouter")
});

app.post("/ajout/fiche", urlencodeparser, function (req, res) {
    //On vérifie si on a déjà un compte avec cet email
    var select = "SELECT pseudo, email, motdepasse FROM users WHERE email = (?);";
    var email = [req.body.email];
    select = mysql.format(select, email);
    db.query(select, function (error, results, fields) {
        if (!error && results.length > 0) {
            res.send("Cette adresse mail est déja utilisée !")
        }
    });
    //On insère le nouveau compte dans la BDD
    var sql = "INSERT INTO users (logo, pseudo, motdepasse, nom, prenom, entreprise, telephone, email, adresse, codepostal, ville, description) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);"
    var values = [req.body.logo, req.body.pseudo, bcrypt.hashSync(req.body.motdepasse, null, null), req.body.nom, req.body.prenom, req.body.entreprise, req.body.telephone, req.body.email, req.body.adresse, req.body.codepostal, req.body.ville, req.body.description];
    sql = mysql.format(sql, values);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            console.log("Oui, ça fonctionne maggle")
            res.redirect("/");
        } else {
            console.log(error)
            res.send(error);
        }
    })
});

//Se connecter avec un compte admin ou membre

app.get("/connexion", function (req, res) {
    res.render("connexion");
});

app.post("/form/connect", urlencodeparser, function (req, res) {
    var sql = "SELECT pseudo, email, motdepasse FROM users WHERE pseudo = (?);";
    var values = [req.body.pseudo];
    sql = mysql.format(sql, values);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            var msg = "Pas de correspondance trouvée en base";
            if (results.length != 1) {
                console.log(results);
                res.send(msg);
            } else {
                if (bcrypt.compareSync(req.body.motdepasse, results[0].motdepasse)) {
                    logged_user = {
                        pseudo: results[0].pseudo,
                        email: results[0].email
                    };
                    message = "Vous êtes bien connecté";
                    console.log("Et oui, ça marche encore !")
                    res.redirect("/menu");
                } else {
                    res.send(msg);
                }
            }
        } else {
            res.send(error);
        }
    })
})



app.listen(3266, function () {
    console.log('le serveur écoute sur le port 3266');
});
