var cacheName = 'legest';
var filesToCache = [
    '/',
    '/style.css',
    '/img/Fond.png',
    '/img/icon-chercher.png',
    '/img/icon-microphone.png',
    '/img/Larry-Galaxie.png',
    '/img/logo_entreprise_90.png',
    '/img/logo_mail_70.png',
    '/img/logo_participants_30.png',
    '/img/logo_participants_50.png',
    '/img/logo_telephone_70.png',
    '/img/logo-1.png',
    '/img/share.png'
];

self.addEventListener('install', function(e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(
        caches.open(cacheName).then(function(caches) {
            console.log('[ServiceWorker] Caching app shell');
            return caches.addAll(filesToCache);
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request, { ignoreSearch: true}).then(response => {
            return response || fetch(event.request);
        })
    );
});